// Imported Elements
import './App.css';
import Hero from './components/Banner.js';
import Navbar from './components/AppNavbar.js';
import Highlights from './components/Highlights.js';
import ProductCard from './components/ProductCard.js';
import Home from './pages/Home.js';
import Products from './pages/Catalog.js';
import Login from './pages/Login.js';
import Register from './pages/Register.js';
import Error from './pages/Error.js';
import Item from './pages/Retrieve.js';

import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import { useState } from 'react';

import {UserProvider} from './UserContext'

// Functions
function App() {
  
// Describe Status of the User Using the App - Using UseState() / Statehook


  const [ user, setUser ] = useState({
  
      id: null,
      isAdmin: null
  
    });

// Function to Clear Out the Information in the Local Storage - Means User Logged Out

  const unsetUser = () => {

    localStorage.clear();
    setUser({

      id: null,
      isAdmin: null

    })

  }

  return (

        <UserProvider value={{user, unsetUser, setUser}}> 
           <Router>
            <Navbar />
              <Switch>
                <Route exact path="/products" component={Products} />   
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/" component={Home} />
                <Route component={Error} />
                <Route exact path="/item/:id" component={Item} />
              </Switch>
           </Router>
        </UserProvider>

      );
    }

    export default App;
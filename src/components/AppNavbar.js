// This component serves as the Navigation Component; User can transition from one place to another

// Imported Elements
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { useContext } from 'react';
import { NavLink } from 'react-router-dom';

// Component Function

function AppNavbar() {
	return(
		<Navbar bg="success" expand="lg" className="p-4">
			<Navbar.Brand className="brandname">
				GamersParadise.ph
			</Navbar.Brand>
			<Navbar.Toggle aria-conrols="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav.Link as={NavLink} to="/" >Home</Nav.Link>
				<Nav.Link as={NavLink} to="/register" >Register</Nav.Link>
				<Nav.Link as={NavLink} to="/login" >Login</Nav.Link>
				<Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>
				<Nav.Link as={NavLink} to="/products" >Products</Nav.Link>
			</Navbar.Collapse>
		</Navbar>
		)
}

export default AppNavbar;
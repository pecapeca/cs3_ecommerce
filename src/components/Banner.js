// Hero Section, using the Bootstrap Grid System

// Imported Elements
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import BannerImage from './bannerimg.jpg'; 

// Create Function Non-ES6 to describe the structure/anatomy of component

function Banner() {

	return(
		<Row>
			<Col className="p-5">
			<h1 className="bannertext">Looking for The Ultimate Gamer Starter Pack?</h1>
			<img src={BannerImage} alt="BannerImage" className="responsive-image"/>
			</Col>
		</Row>
	)

}

export default Banner;
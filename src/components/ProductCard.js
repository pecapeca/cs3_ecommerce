// Used to display the information about a single product

import Card from 'react-bootstrap/Card';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// Functions

function ProductCard({productInfo}) {
	
	const {_id, name, description, price} = productInfo

	const select = (event) => {
		event.preventDefault()

	}
	console.log(productInfo._id);
	return(
		<div>

		<Card>
			<Card.Body>
				<Card.Title> { name }</Card.Title>
				<Card.Subtitle> { description } </Card.Subtitle>
				<Card.Subtitle> Product Price </Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>

			</Card.Body>
		</Card>

		<Button as={Link} to={`/item/${_id}`} type="submit" id="submitBtn" variant="success" className="btn btn-block" className="m-3"> Details </Button>

		</div>

	)

}

export default ProductCard;
// Imported Elements

import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// Functions

function Highlights() {
	return(
		<Row className="mt-3 mb-3 p-5">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							PC Components at Competitive Prices
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis iste aspernatur nesciunt impedit illo?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							Same Day Delivery
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis iste aspernatur nesciunt impedit illo?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							Graphics Cards at near-MSR Prices
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis iste aspernatur nesciunt impedit illo?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>


		</Row>

	)
}

export default Highlights;
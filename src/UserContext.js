// The purpose of this document is to allow us to create a context object to declare a state about the status of the user/client for our entire application

import React from 'react';

const UserContext = React.createContext()

export const UserProvider = UserContext.Provider

export default UserContext;

// Steps

	// 1. Import React Library
	// 2. Acquire createContext function
	// 3. Identify Provider Component - responsible in allowing 

// Notes

	// The context that we will try to declare will describe the status of the user/client

	// 2 Possible Status:
		// Authenticated/Login
		// Unauthenticated/Logout

	// Provider supplies the information
	// Consumer uses/needs the inormation from the provider
	// UserConetxt - connects the provider and consumer
// The purpose of this document is to provide as response or component that will be sent back to the client. If the client ever tries to access a route within that app which does not exist.

import Hero from '../components/Banner.js';
import { Container } from 'react-bootstrap';

const errorMessage = {
	title: "Page Not Found",
	tagline: "No Such Page Mate"
}

function Error () {
	return(
		
		<Container>
			
			<Hero data={errorMessage} />	

		</Container>

		)
}

export default Error;
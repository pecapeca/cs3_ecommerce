// Imported Components
import Hero from '../components/Banner.js'
import ProductCard from '../components/ProductCard.js'
import { useEffect, useState} from 'react';

// Functions
function Products() {

	const [products, setProducts] = useState([]);
	useEffect(()=>{

	fetch('https://tranquil-dawn-86635.herokuapp.com/products/active').then(fetchOutcome => fetchOutcome.json()).then(convertedData => {
			console.log(convertedData);

			setProducts(convertedData.map(product => {

				return(
					<ProductCard key={product._id} productInfo={product}/>
					)

			}))
		})

	},[])

	return(
	<>
		<Hero />
		{products}
	</>
	)
}


export default Products;
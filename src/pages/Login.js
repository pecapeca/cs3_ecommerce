// Accessing Account

import { Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner.js';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {Redirect} from 'react-router';

import UserContext from '../UserContext';

// Declare component as Consumer

let loginBannerInfo = {
	title: "Sign-In",
	tagline: "This is where you'll sign in"
}

// Functions
function Login() {
	
	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const authenticate = (e) => {
		e.preventDefault()

		fetch(`https://tranquil-dawn-86635.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				email: email,
				password: password
			})


		})
		.then(res => res.json())
		.then(ConvertedInformation => {console.log(ConvertedInformation)
			

			// Response Accdng to Data Sent by the Server
			localStorage.setItem('accessToken', ConvertedInformation.accessToken);
			if (typeof ConvertedInformation.accessToken !== "undefined") {
				retrieveUserDetails(ConvertedInformation.accessToken);
				Swal.fire({
				title: 'Login Successful!',
				icon: 'success',
				text: 'Welcome to the App!'
				});
			} else {
				Swal.fire({
				title: 'Auth Failed!',
				icon: 'error',
				text: 'Check your login details and try again!'
				});
			}
		})
	}


	// Function to Retrieve User Details

	const retrieveUserDetails = (token) => {
		fetch('https://tranquil-dawn-86635.herokuapp.com/users/exists', {
			headers: {
				Authorization: `Bearer ${token}` // Returns a Concatenated Response
			} 
		}).then(resultOfPromise => resultOfPromise.json()).then(convertedResult => {console.log(convertedResult);

			// Since this is the function that gets the informatino about the user, let's use this function to change the current Global state of the user too

			setUser({
				id: convertedResult._id, 
				isAdmin: convertedResult.isAdmin
			});
		})

	}



	// Let's redirect the user if authentication is successful, let's do that by evaluating the user's current state

	// Use Effect
	useEffect(() => {
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);


	// Create a ternary structure inside the return to det the proced that will happen if a user is found

	return(

		(user.id !== null) ? 

		<Redirect to="/products"/> 
		: 
		

		<Container>
		
			<Hero data={loginBannerInfo}/>

			{/*Start of Login Form Component*/}

			<Form className="mb-5" onSubmit={e => authenticate(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label> Email Address: </Form.Label>
					<Form.Control 
					type="email" 
					placeholder="Insert E-Mail Here" 
					value={email}
					onChange={e => setEmail(e.target.value)}
					required/>
				</Form.Group>


				<Form.Group controlId="Password">
					<Form.Label> Password: </Form.Label>
					<Form.Control 
					type="password" 
					placeholder="Input Password Here" 
					value={password}
					onChange={e => setPassword(e.target.value)}
					required/>
				</Form.Group>
			

			{/*Start of Button Component*/}

			{
				isActive ?

				<Button type="submit" id="submitBtn" variant="success" className="btn btn-block" > Login </Button>
				:
				<Button type="submit" id="submitBtn" variant="success" className="btn btn-block" disabled> Login </Button>
			}

			

			</Form>		

		</Container>
		);
}

export default Login;

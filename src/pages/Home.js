// Entry Point of the Domain
import Hero from '../components/Banner.js';
import Highlights from '../components/Highlights.js';
import Container from 'react-bootstrap/Container';

// Functions

function Home () {
	return(
	<>
		<Hero />
		<Highlights />
	</>
	)
}

export default Home;
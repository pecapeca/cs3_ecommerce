// Imported Elements

import { useState, useEffect } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner.js';
import Swal from 'sweetalert2';
import {Redirect, useHistory} from 'react-router-dom';


const registrationBanner = {
	title: "Create Your Own Account Here!",
	tagline: "The Best Deals, Just Waiting for Ya!"
}


function Register() {

// State Hooks
// Syntax const [ stateGetter, stateSetter] = useState(false);

const history = useHistory();

const [ firstName, setFirstName ] = useState(''); 
const [ middleName, setMiddleName ] = useState('');
const [ lastName, setLastName ] = useState('');
const [ email, setEmail ] = useState('');
const [ password1, setPassword1 ] = useState('');
const [ password2, setPassword2 ] = useState('');
const [ mobileNo, setMobileNo ] = useState('');

const [ gender, setGender ] = useState('Male');

const [ isRegisterBtnActive, setRegisterBtnActive ] = useState('');

// Reactive Components

const [ isComplete, setIsComplete ] = useState(false);
const [ isPasswordsMatch, setIsPasswordsMatch ] = useState(false);


// Start of REGISTER PROCESS SIMULATION

function registerUser(event) {
	event.preventDefault();

	// Data Checkers

		console.log(firstName);
		console.log(middleName);
		console.log(lastName);
		console.log(email);
		console.log(mobileNo);
		console.log(password1);
		console.log(gender);

	
	// fetch('https://blooming-garden-28660.herokuapp.com/users/register')


	fetch(`https://tranquil-dawn-86635.herokuapp.com/users/register`, {method: "POST", headers: {
			'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		firstName: firstName,
		middleName: middleName,
		lastName: lastName,
		email: email,
		password: password1,
		mobileNo: mobileNo,
		gender: gender
	})

	}).then(response => response.json()).then(data => {
		// console.log(data);
		Swal.fire({
			title: `Hey ${firstName}, your account has been registered successfully`,
			icon: 'success',
			text: 'Welcome to Zuitt'
		});

		history.push('/login'); // Redirects newly-registered user to login page after he has registered

	})

}




// Side Effect for Button Enabling

useEffect(() => {

	if (firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') {
		setRegisterBtnActive(true);
		setIsComplete(true);
		setIsPasswordsMatch(true);
	} else {
		setRegisterBtnActive(false);
		setIsComplete(false);
		setIsPasswordsMatch(false);
	}
},[firstName, middleName, lastName, email, password1, password2, mobileNo])

useEffect(() => {
	if (password1 === password2 && password1 !== '')
	{
		setIsPasswordsMatch(true);
	} else {
		setIsPasswordsMatch(false);
	}
});

	return(
		<Container>
			<Hero data={registrationBanner} />

			{ isComplete ?
				<h2 className="mb-5"> Proceed with Register </h2>
				:
				<h2 className="mb-5"> Please Provide the Following Information </h2>
				
			}

			
			

			{/*Start of Register Form Component*/}

			<Form onSubmit={(event) => registerUser(event)} className="mb-5">
			
			{/*Start of First Name*/}
				<Form.Group controlId="firstName">
					<Form.Label>
						First Name:
					</Form.Label>
					<Form.Control type="text" placeholder="Insert First Name Here" value={firstName} 
					onChange={event => setFirstName(event.target.value)} required/>
				</Form.Group>

			{/*Start of Middle Name*/}
				<Form.Group controlId="middleName">
					<Form.Label>
						Middle Name:
					</Form.Label>
					<Form.Control type="text" placeholder="Insert Middle Name Here" value={middleName} onChange={event => setMiddleName(event.target.value)} required/>
				</Form.Group>

			{/*Start of Last Name*/}
				<Form.Group controlId="lastName">
					<Form.Label>
						Last Name:
					</Form.Label>
					<Form.Control type="text" placeholder="Insert Last Name Here" value={lastName} onChange={event => setLastName(event.target.value)} required/>
				</Form.Group>

			{/*Start of Email Address*/}
				<Form.Group controlId="userEmail">
					<Form.Label>
						Email:
					</Form.Label>
					<Form.Control type="text" placeholder="Insert Your E-Mail Here" value={email} onChange={event => setEmail(event.target.value)} required/>
				</Form.Group>

			{/*Start of Password 1*/}
				<Form.Group controlId="password1">
					<Form.Label>
						Password:
					</Form.Label>
					<Form.Control type="password" placeholder="Insert Your Password Here" value={password1} onChange={event => setPassword1(event.target.value)} required/>
				</Form.Group>

				{ isPasswordsMatch ?
					<p className="text-success"> *Passwords Match!* </p>
					:
					<p className="text-danger"> *Passwords Should Match!* </p>
				}

				
				

			{/*Start of Password Verification*/}
				<Form.Group controlId="password2">
					<Form.Label>
						Password Verification:
					</Form.Label>
					<Form.Control type="password" placeholder="Retype Your Password Here" value={password2} onChange={event => setPassword2(event.target.value)} required/>
				</Form.Group>

			{/*Gender using Form.Control*/}

				<Form.Group controlId="gender">
					<Form.Label>
						Select Gender:
					</Form.Label>
					<Form.Control as="select" value={gender} onChange={(e) => setGender(e.target.value)}>
						<option selected disabled>Select Gender Here</option>
						<option value="Male">Male</option>
						<option value="Female">Female</option>
					</Form.Control>
				</Form.Group>

			{/*Mobile Number*/}
				<Form.Group controlId="mobilenumber">
					<Form.Label>
						Mobile Number:
					</Form.Label>
					<Form.Control required type="number" maxLength={11} placeholder="Type your Phone Number Here" value={mobileNo} onChange={event => setMobileNo(event.target.value)} required/>
				</Form.Group>

{/*Interactive Button - Will only be enabled if user enters complete details*/}
			{isRegisterBtnActive ? 
			<Button variant="warning" className="btn btn-block" type="submit" id="submitBtn">
					Create New Account
			</Button>
			:
			<Button variant="danger" className="btn btn-block" type="submit" id="submitBtn" disabled>
				Create New Account
			</Button>
			}
			
			</Form>


		</Container>
		)
}

export default Register;